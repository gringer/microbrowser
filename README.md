Microarray Data Browser App
-----

Features:

* Loading probe data from 4 different preexisting array types + pre-converted genes
* Overall gene expression histogram
* Individual gene expression scatter plot / strip chart
* Multi-gene heatmap (both pheatmap and ggplot versions)
* Save to PDF
* Save heatmap data to CSV
